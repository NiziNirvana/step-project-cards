const modalEmail = document.querySelector('.input-email');
const modalPass = document.querySelector('.input-password');
const createBtn = document.querySelector('.header-top-create');

const loginForm = document.querySelector("#login-form");
const greetingMsg = document.querySelector("#greeting");

loginForm.addEventListener("submit", async function (event) {
    event.preventDefault();
    const accessToken = await getToken(modalEmail.value, modalPass.value);
    if (validateToken(accessToken)) {
        console.log("==> INFO: Login Success!");

        modalWindow.classList.remove("open");
        
        localStorage.setItem('activeUser', JSON.stringify(new User(modalEmail.value, modalPass.value, accessToken)));
        activeUser = JSON.parse(localStorage.getItem('activeUser'));
        await fillCards();
        renderCards();

        if (localStorage.length < 2) {
            document.querySelector("#cardsInfo").innerHTML = "Пока не создано ни одной карточки."
        }

        loggedIn();
    } else {
        console.log(`==> ERROR: Wrong token for user '${modalEmail.value}' with password ${modalPass.value}`);
        alert("Неверная почта или пароль! Попробуйте снова.")
    }

});

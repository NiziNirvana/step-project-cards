const editCloseBtn = document.querySelector("#button-edit-cancel");
const editCardForm = document.querySelector("#edit-card-form");

const editModalWindow = document.querySelector(".modal-edit");

function showEditForm(id) {
    if (! editModalWindow.classList.contains("open")) {
        editCardForm.querySelector("h3").innerHTML = `Редактирование карточки #${id}` ;
        editModalWindow.classList.add("open");
        setValues(editCardForm, getPreEditValues(id));
    } else {
        editModalWindow.classList.remove("open")
    }
}

editCloseBtn.addEventListener("click", function (){
    editModalWindow.classList.remove("open");
});


editCardForm.addEventListener("submit", async function(event) {
    event.preventDefault();
    let newCard = await editCard(event.target.querySelector('h3').textContent.split('#').pop());
    localStorage.removeItem(`card-${newCard.id}`);
    localStorage.setItem(`card-${newCard.id}`, JSON.stringify(newCard));
    editModalWindow.classList.remove("open");
    renderCards();
});
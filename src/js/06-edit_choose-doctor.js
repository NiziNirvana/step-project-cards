let editDoctorSelect = document.querySelector("#visit_edit_doctor");
let editPressureBlock = document.querySelector("#edit_pressure");
let editMassIdBlock = document.querySelector("#edit_massId");
let editIllnessesBlock = document.querySelector("#edit_illnesses");
let editAgeBlock = document.querySelector("#edit_age");
let editLastVisitBlock = document.querySelector("#edit_lastVisit");

editDoctorSelect.addEventListener("change", function(event) {
    if (event.target.value === "Кардиолог") {
        setOption(editPressureBlock, "show");
        setOption(editMassIdBlock, "show");
        setOption(editIllnessesBlock, "show");
        setOption(editAgeBlock, "show");
        setOption(editLastVisitBlock, "hide");
    } else if (event.target.value === "Стоматолог") {
        setOption(editPressureBlock, "hide");
        setOption(editMassIdBlock, "hide");
        setOption(editIllnessesBlock, "hide");
        setOption(editAgeBlock, "hide");
        setOption(editLastVisitBlock, "show");
    } else if (event.target.value === "Терапевт") {
        setOption(editPressureBlock, "hide");
        setOption(editMassIdBlock, "hide");
        setOption(editIllnessesBlock, "hide");
        setOption(editAgeBlock, "show");
        setOption(editLastVisitBlock, "hide");
    }
});
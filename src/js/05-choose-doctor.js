let doctorSelect = document.querySelector("#visit_doctor");
let pressureBlock = document.querySelector("#pressure");
let massIdBlock = document.querySelector("#massId");
let illnessesBlock = document.querySelector("#illnesses");
let ageBlock = document.querySelector("#age");
let lastVisitBlock = document.querySelector("#lastVisit");

doctorSelect.addEventListener("change", function(event) {
    if (event.target.value === "Кардиолог") {
        setOption(pressureBlock, "show");
        setOption(massIdBlock, "show");
        setOption(illnessesBlock, "show");
        setOption(ageBlock, "show");
        setOption(lastVisitBlock, "hide");
    } else if (event.target.value === "Стоматолог") {
        setOption(pressureBlock, "hide");
        setOption(massIdBlock, "hide");
        setOption(illnessesBlock, "hide");
        setOption(ageBlock, "hide");
        setOption(lastVisitBlock, "show");
    } else if (event.target.value === "Терапевт") {
        setOption(pressureBlock, "hide");
        setOption(massIdBlock, "hide");
        setOption(illnessesBlock, "hide");
        setOption(ageBlock, "show");
        setOption(lastVisitBlock, "hide");
    }
});
const createLoginBtn = document.querySelector('.header-top-create');
const createModalWindow = document.querySelector(".modal-create");
const closeBtn = document.querySelector("#button-create-cancel");
const cardForm = document.querySelector("#card-form");

closeBtn.addEventListener("click", function (){
    createModalWindow.classList.remove("open");
});

createLoginBtn.addEventListener("click", function() {
    if (! createModalWindow.classList.contains("open")) {
        createModalWindow.classList.add("open");
    } else {
        createModalWindow.classList.remove("open")
    }
    console.log(activeUser.token);
});

cardForm.addEventListener("submit", async function(event) {
    event.preventDefault();
    let newCard = await createCard(activeUser.token, cardForm);
    localStorage.setItem(`card-${newCard.id}`, JSON.stringify(newCard));
    createModalWindow.classList.remove("open");
    renderCards();
    searchByDesc();
    filterBySeverity();
});
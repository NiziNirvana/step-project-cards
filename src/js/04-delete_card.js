async function deleteCard(id) {  
  await fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
    method: 'DELETE',
    headers: {
      'Authorization': `Bearer ${activeUser.token}`
    },
  });

  localStorage.removeItem(`card-${id}`);
  renderCards();
  searchByDesc();
  filterBySeverity();
}
  
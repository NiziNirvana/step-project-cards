async function createCard(token, cardForm) {
    let jsonReq = '{';
    [...cardForm.querySelectorAll(".optionBlock:not(.hidden)")].forEach(function (el) {
        let val = el.querySelector(`#visit_${el.id}`).value;
        if (val) {
            jsonReq += `"${el.id}":"${val}",`;
        }
    });
    jsonReq = jsonReq.slice(0, -1);
    jsonReq += '}';
    
    let cardObject = null;

    try {
        await fetch("https://ajax.test-danit.com/api/v2/cards", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
        },
        body: jsonReq,
        })
            .then((response) => response.json())
            .then((response) => {
                if (response.doctor === "Терапевт") {
                    cardObject = new TherapistCard(response.doctor, response.goal, response.desc, response.severity, response.fio, response.age, response.id)
                } else if (response.doctor === "Стоматолог") {
                    cardObject = new DentistCard(response.doctor, response.goal, response.desc, response.severity, response.fio, response.lastVisit, response.id)
                } else {
                    cardObject = new CardiologistCard(response.doctor, response.goal, response.desc, response.severity, response.fio, response.pressure, response.massId, response.illnesses, response.age, response.id)
                }
            })
    
        return cardObject;
    } catch(error) {
        console.log(`==> ERROR: ${error}`);
        return null;
    }
  }
  
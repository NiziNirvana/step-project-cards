let activeUser = JSON.parse(localStorage.getItem('activeUser'));

async function start() {
    if (activeUser) {
        if (validateToken(activeUser.token)) {
            await fillCards();
            renderCards();
            loggedIn();
        }
    }
}

window.onload = start();

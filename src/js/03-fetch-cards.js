class Card {
  constructor(doctor, goal, desc, severity, fio, id) {
    this.doctor = doctor;
    this.goal = goal;
    this.description = desc;
    this.severity = severity;
    this.fio = fio;
    this.id = id;
  }
  printHTML() {
    let result = ` <div class="card " id="card-${this.id}">
        <div class="card_header d9" id="card_header-${this.id}">
            <h3>Запись #${this.id}</h3>
            <button id="button_delete-${this.id}" class="always_visible buttonX" onclick="deleteCard(${this.id});">x</button>
        </div><br> `;
    for (let field in this) {
      let prefix = "";
      if (field === "doctor") {
        prefix = "Врач: ";
      } else if (field === "goal") {
        prefix = "Цель визита: ";
      } else if (field === "description") {
        prefix = "Краткое описание: ";
      } else if (field === "severity") {
        prefix = "Срочность: ";
      } else if (field === "fio") {
        prefix = "ФИО: ";
      } else if (field === "pressure") {
        prefix = "Обычное давление: ";
      } else if (field === "massId") {
        prefix = "Индекс массы тела:  ";
      } else if (field === "illnesses") {
        prefix = "Перенесенные заболевания сердечно-сосудистой системы: ";
      } else if (field === "age") {
        prefix = "Возраст: ";
      } else if (field === "lastVisit") {
        prefix = "Дата последнего посещения: ";
      } else {
        prefix = null;
      }
      if (prefix) {
        if (field === "doctor" || field === "fio") {
          result += `<p id=${field}-${this.id}>${prefix}${this[field]}</p>`;
        } else {
          result += `<p id=${field}-${this.id} class='hidden'>
                        ${prefix}${this[field]}
                    </p>`;
        }
      }
    }
    result += ` <br>
        <div class="card-buttons">
            <button id="button_unShowMore-${this.id}" class="btn hidden buttonEdit" onclick="unShowMore(${this.id});">Скрыть подробности</button>
            <button id="button_showMore-${this.id}" class="btn buttonEdit" onclick="showMore(${this.id});">Показать больше</button>
            <button id="button_edit-${this.id}" class="btn always_visible buttonEdit" onclick="showEditForm(${this.id});">Редактировать</button>
        </div>`;
    return result;
  }
}

class CardiologistCard extends Card {
  constructor(
    doctor,
    goal,
    desc,
    severity,
    fio,
    pressure,
    massId,
    illnesses,
    age,
    id
  ) {
    super(doctor, goal, desc, severity, fio, id);
    this.pressure = pressure;
    this.massId = massId;
    this.illnesses = illnesses;
    this.age = age;
  }
}

class TherapistCard extends Card {
  constructor(doctor, goal, desc, severity, fio, age, id) {
    super(doctor, goal, desc, severity, fio, id);
    this.age = age;
  }
}

class DentistCard extends Card {
  constructor(doctor, goal, desc, severity, fio, lastVisit, id) {
    super(doctor, goal, desc, severity, fio, id);
    this.lastVisit = lastVisit;
  }
}

async function fetchCards(token) {
  let rList = [];
  try {
    await fetch("https://ajax.test-danit.com/api/v2/cards", {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((response) => response.json())
      .then((response) => (rList = response));
    let res = rList;

    return res;
  } catch (error) {
    console.log(`==> ERROR: ${error}`);
    return [];
  }
}

async function fillCards() {
  let cardsList = [];
  let jsonCardsList = await fetchCards(activeUser.token);
  Object.entries(jsonCardsList).forEach(function (el) {
    if (el[1].doctor === "Кардиолог") {
      cardsList.push(
        new CardiologistCard(
          el[1].doctor,
          el[1].goal,
          el[1].description,
          el[1].severity,
          el[1].fio,
          el[1].pressure,
          el[1].massId,
          el[1].illnesses,
          el[1].age,
          el[1].id
        )
      );
    } else if (el[1].doctor === "Терапевт") {
      cardsList.push(
        new TherapistCard(
          el[1].doctor,
          el[1].goal,
          el[1].description,
          el[1].severity,
          el[1].fio,
          el[1].age,
          el[1].id
        )
      );
    } else {
      cardsList.push(
        new DentistCard(
          el[1].doctor,
          el[1].goal,
          el[1].description,
          el[1].severity,
          el[1].fio,
          el[1].lastVisit,
          el[1].id
        )
      );
    }
  });
  cardsList.forEach(function (el) {
    localStorage.setItem(`card-${el.id}`, JSON.stringify(el));
  });
}

function printCards() {
  //await fillCards();
  let cardsList = [];
  for (let i = 0; i < localStorage.length; i++) {
    if (!JSON.parse(localStorage.getItem(localStorage.key(i))).token) {
      cardsList.push(JSON.parse(localStorage.getItem(localStorage.key(i))));
    }
  }

  return cardsList.map(function (el) {
    if (el.doctor === "Кардиолог") {
      return new CardiologistCard(
        el.doctor,
        el.goal,
        el.description,
        el.severity,
        el.fio,
        el.pressure,
        el.massId,
        el.illnesses,
        el.age,
        el.id
      );
    } else if (el.doctor === "Стоматолог") {
      return new DentistCard(
        el.doctor,
        el.goal,
        el.description,
        el.severity,
        el.fio,
        el.lastVisit,
        el.id
      );
    } else {
      return new TherapistCard(
        el.doctor,
        el.goal,
        el.description,
        el.severity,
        el.fio,
        el.age,
        el.id
      );
    }
  });
}

function renderCards() {
  console.log("==> INFO: Rendering cards list");
  document.querySelector("#cardsInfo").innerHTML = "";
  let cardsList = printCards();
  cardsList.forEach(function (el) {
    document.querySelector("#cardsInfo").innerHTML += el.printHTML();
  });
  if (localStorage.length < 2) {
    document.querySelector("#cardsInfo").innerHTML =
      "Пока не создано ни одной карточки.";
  }
}
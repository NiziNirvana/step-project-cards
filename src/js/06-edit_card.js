function getPreEditValues(id) {
    let values = [];
    document.querySelectorAll(`#card-${id} p`).forEach(function (el) {
        values.push(el.textContent.split(':').pop().slice(1).trim());
    })
    return values;
}

function setValues(form, values) {
    // set doctor
    let doctorSelect = form.querySelector('#visit_edit_doctor');
    doctorSelect.value = values[0];
    doctorSelect.dispatchEvent(new Event('change'));

    //set all fields
    form.querySelectorAll(".optionBlock:not(.hidden)").forEach(function (el, index) {
        el.querySelector(`#visit_${el.id}`).value = values[index];
    })
}

async function editCard(id) {
    let jsonReq = '{';

    [...editCardForm.querySelectorAll(".optionBlock:not(.hidden)")].forEach(function (el) {
        let val = el.querySelector(`#visit_${el.id}`).value.trim();
        if (val) {
            jsonReq += `"${el.id.split('_').pop()}":"${val}",`;
        }
    });
    jsonReq = jsonReq.slice(0, -1);
    jsonReq += '}';
    
    let cardObject = null;

    try {
        await fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${activeUser.token}`,
        },
        body: jsonReq,
        })
            .then((response) => response.json())
            .then((response) => {
                if (response.doctor === "Терапевт") {
                    cardObject = new TherapistCard(response.doctor, response.goal, response.desc, response.severity, response.fio, response.age, response.id)
                } else if (response.doctor === "Стоматолог") {
                    cardObject = new DentistCard(response.doctor, response.goal, response.desc, response.severity, response.fio, response.lastVisit, response.id)
                } else {
                    cardObject = new CardiologistCard(response.doctor, response.goal, response.desc, response.severity, response.fio, response.pressure, response.massId, response.illnesses, response.age, response.id)
                }
            })
    
        return cardObject;
    } catch(error) {
        console.log(`==> ERROR: ${error}`);
        return null;
    }
  }
function loggedIn() {
    loginBtn.classList.add("hidden");
    logoutBtn.classList.remove("hidden");
    createBtn.classList.remove("hidden");
    document.querySelector("#cardsInfo").classList.remove("hidden");

    greetingMsg.innerHTML = `Вы вошли как ${activeUser.email}`;
    greetingMsg.classList.remove("hidden");
}

function loggedOut() {
        loginBtn.classList.remove("hidden");
        logoutBtn.classList.add("hidden");
        createBtn.classList.add("hidden");
        document.querySelector("#cardsInfo").classList.add("hidden");

        greetingMsg.innerHTML = "";
        greetingMsg.classList.add("hidden");
}

async function getToken(email, pwd) {
    let rToken = null;
    try {
        await fetch("https://ajax.test-danit.com/api/v2/cards/login", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({email: email, password: pwd})
        })
            .then(response => response.text())
            .then(token => rToken = token)
        let res = rToken;

        return res;
    } catch(error) {
        console.log(`==> ERROR: ${error}`);
        return null;
    }
}

function validateToken(token) {
    return token.match(/^(.{8})-(.{4})-(.{4})-(.{12})/);
}

function setOption(el, state) {
    if (state === "hide") {
        el.classList.add("hidden");
        el.querySelector(`#visit_${el.id}`).required = false;
    } else {
        el.classList.remove("hidden");
        el.querySelector(`#visit_${el.id}`).required = true;
    }
}

function closeWindowIfOpened(modalWin) {
    if (modalWin.classList.contains("open")) {
        modalWin.classList.remove("open");
    }
}
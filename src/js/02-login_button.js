const loginBtn = document.querySelector('.header-top-login');
const modalWindow = document.querySelector(".modal-login-password");

loginBtn.addEventListener("click", function() {
    if (! modalWindow.classList.contains("open")) {
        modalWindow.classList.add("open");
    }
});
function searchByDesc() {
    renderCards();
    let input, filter, cards, goal, desc, i, txtValue;
    input = document.getElementById('searchHeading');
    filter = input.value.toUpperCase();
    cards = document.querySelectorAll('.card:not(.hidden)');

    for (i = 0; i < cards.length; i++) {
        goal = cards[i].getElementsByTagName('p')[1];
        desc = cards[i].getElementsByTagName('p')[2];
        txtValue = goal.textContent.split(':').pop().trim() + ' ' + desc.textContent.split(':').pop().trim();
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            cards[i].classList.remove("hidden");
        } else {
        cards[i].classList.add("hidden");
        }
    }
    if (document.querySelectorAll('.card:not(.hidden)').length < 1 && filter && localStorage.length > 1) {
        document.querySelector("#cardsInfo").innerHTML = "Не найдено карточек, удовлетворяющих запросам фильтров."
    }
}

function filterBySeverity() {
    renderCards();
    let select, filter, cards, p, i, txtValue;
    select = document.getElementById('visit_filter_severity');
    filter = select.value.toUpperCase();
     if (filter === "ЛЮБАЯ") {
          filter = '';
    }
    cards = document.querySelectorAll('.card:not(.hidden)');

    for (i = 0; i < cards.length; i++) {
        p = cards[i].getElementsByTagName('p')[3];
        txtValue = p.textContent || p.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            cards[i].classList.remove("hidden");
        } else {
        cards[i].classList.add("hidden");
        }
    }
    if (document.querySelectorAll('.card:not(.hidden)').length < 1 && filter && localStorage.length > 1) {
        document.querySelector("#cardsInfo").innerHTML = "Не найдено карточек, удовлетворяющих запросам фильтров."
    }
}
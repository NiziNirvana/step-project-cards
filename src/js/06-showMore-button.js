function showMore(id) {
    document.querySelector(`#card-${id}`) .querySelectorAll("p.hidden,button.hidden").forEach(function (el) {
        el.classList.remove("hidden");
    });
    document.querySelector(`#button_showMore-${id}`).classList.add("hidden");
    document.querySelector(`#card-${id}`).classList.add("unfolded");
}

function unShowMore(id) {
    document.querySelector(`#card-${id}`).querySelectorAll("p:not(.hidden),button:not(.hidden,.always_visible)").forEach(function (el) {
        if (el.id !== `doctor-${id}` && el.id !== `fio-${id}`) el.classList.add("hidden");
    });
    document.querySelector(`#button_showMore-${id}`).classList.remove("hidden");
    document.querySelector(`#card-${id}`).classList.remove("unfolded");
}
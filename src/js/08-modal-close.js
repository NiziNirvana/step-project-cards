window.onkeydown = function(event) {
    if (event.keyCode == 27) {
        console.log(`==> DEBUG: ESC Pressed`);
        closeWindowIfOpened(modalWindow);
        closeWindowIfOpened(createModalWindow);
        closeWindowIfOpened(editModalWindow);
    }
}

window.onclick = function(event) {
    console.log("==> DEBUG: Captured click!");
    console.log(event.target);

    if (event.target == modalWindow) {
        console.log("==> DEBUG: Closing Login Form!")
        closeWindowIfOpened(modalWindow);
    }
    if (event.target == createModalWindow) {
        console.log("==> DEBUG: Closing Create Card Form!");
        closeWindowIfOpened(createModalWindow);
    }
    if (event.target == editModalWindow) {
        console.log("==> DEBUG: Closing Edit Card Form!")
        closeWindowIfOpened(editModalWindow);
    }
 }